#include <stdio.h>
#include <iostream>
#include "myVector.h"

using namespace std;

int main()
{
	MyVector v;
	MyVector v1(1);
	cout << v.Size() << " " << v1.Size() << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << v.At(i) << endl;
	}
	for (int i = 0; i < 10; i++)
	{
		v.At(i) = i;
	}
	try
	{
		for (int i = 0; i < 20; i++)
		{
			cout << v.At(i) << endl;
		}
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}

	for (int i = 0; i < 10; i++)
	{
		cout << v[i] << endl;
	}

	system("pause");
	return 0;
}