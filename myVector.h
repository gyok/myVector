#pragma once

class MyVector
{
	int* mp_head;
	unsigned int mp_size;
	unsigned int mp_capacity;
public:
	MyVector(unsigned int);
	MyVector();
	~MyVector();

	int Size();
	int& Front();
	int& Back();
	bool Empty();
	int& operator[](unsigned int);
	int& At(unsigned int);
	void Resize(unsigned int);
	void PushBack(const int&);
};

