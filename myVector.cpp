#include "myVector.h"
#include <cstdlib>
#include <stdexcept>


MyVector::MyVector(unsigned int i_count)
	: mp_size{i_count}
	, mp_capacity{i_count}
{
	mp_head = static_cast<int*>(std::malloc(mp_size * sizeof(int)));
}


MyVector::MyVector()
  : MyVector::MyVector(10)
{
}


MyVector::~MyVector()
{
}


int MyVector::Size()
{
	return mp_size;
}


int& MyVector::Front()
{
	return *mp_head;
}


int& MyVector::Back()
{
	return *(mp_head + this->Size() - 1);
}


bool MyVector::Empty()
{
	return this->Size() == 0;
}


int& MyVector::operator[](unsigned int i_pos)
{
	return *(mp_head + i_pos);
}


int& MyVector::At(unsigned int i_pos)
{
	if (i_pos < this->Size()) {
		return this->operator[](i_pos);
	}
	throw std::out_of_range("MyVector is not so big");
}


void MyVector::Resize(unsigned int i_new_size)
{
	const unsigned int& copy_count = mp_capacity < i_new_size
		? mp_capacity
		: i_new_size;
}


void MyVector::PushBack(const int& i_elem)
{
	if (mp_capacity > Size())
	{
		this[Size()] = std::move(i_elem);
	}
	// continue
}